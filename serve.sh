
mkdir -p public/
cp serve.el public/

guix time-machine -C channels.scm                 \
     -- environment -C -N -m manifest.scm -E TERM \
     --no-cwd --expose=./public=$HOME                  \
     -- emacs -l $HOME/serve.el
