
CSS="sassc -I css src/css/org.scss > src/css/org.css"
POSTS="emacs --batch -l publish.el -f org-publish-all"

guix time-machine -C channels.scm \
     -- environment -C -m manifest.scm \
     -- sh -c "$CSS; $POSTS;"
